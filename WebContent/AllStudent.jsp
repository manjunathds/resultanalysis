<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@	page import="java.sql.*,
				 java.lang.*,
				 java.text.SimpleDateFormat,
				 java.util.*,
				 java.io.*,
				 javax.servlet.*,
				 javax.servlet.http.*"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin</title>
<link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="js/datatable/css/jquery.dataTables.css" rel="stylesheet">
<link href="js/datatable/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/bootstrap/js/bootstrap.min.js"></script>
<script src="js/jstorage.js"></script>
<script src="js/underscore-min.js"></script>
<script src="js/datatable/js/jquery.dataTables.min.js"></script>
<script src="js/datatable/js/dataTables.bootstrap.js"></script>

</head>
<body>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>All Students Results of a Semester</h1>
            </div>
        </div>
        <!-- /.row -->
		 <div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<div class="row" id="mst-table"> 
				<table id="userInfoTable" class="table table-condensed table-bordered table-hover">
					<thead>
						<tr>
							<th class="col-xs-2">Register No</th>
							<th class="col-xs-4">Name</th>
							<th class="col-xs-2">Internal Total</th>
							<th class="col-xs-2">External Total</th>
							<th class="col-xs-1">Total</th>
							<th class="col-xs-2">Result</th>
						</tr>
					</thead>
				</table>	
			</div>
			</div>
		
		</div>	
    </div>
    <!-- /.container -->

 
<script type="text/javascript">
$(document).ready(function(){
	
	
	var toppersData = $.jStorage.get("topper_result");
	var sortedData  = _.sortBy(toppersData,function(obj){return (+obj.total + +obj.iaTotal)}).reverse();
	//var top10Data	= sortedData.length>0?sortedData.slice(0,10):sortedData;
		var iaTotal = null,extTotal=null,total=null;
		$("#userInfoTable").dataTable({
			"bPaginate" : true,
			"bDestroy" : true,
			"bProcessing" : false,
			"bSort": false,
			"bAutoWidth" : false,
			"iDisplayLength" : 15,
			'bLengthChange' : false,
			"aoColumns" : [{
					"mData" : "regNo"
				}, {
					"mData" : "name",
				}, {
					"mData" : "iaTotal",
					"mRender":function(data,type,full){
						iaTotal = data;
						return iaTotal;
					}
				}, {
					"mData" : "total",
					"mRender":function(data,type,full){
						extTotal = +data - +iaTotal;
						return extTotal;
					}
				}, {
					"mData" : "total",
					"mRender":function(data,type,full){
						total = +iaTotal + +extTotal;
						return total;
					}
				}, {
					"mData" : "result"
				}
			],
			aaData : sortedData
		});
	
});
 
</script>   
</body>
</html>