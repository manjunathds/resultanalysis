<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jstorage.js"></script>
<script src="js/underscore-min.js"></script>
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<script type="text/javascript">
$(function () {
	
	var result  		= $.jStorage.get("result");
	var selSubject 		= $.jStorage.get("subject");
	var selIndex 		= $.jStorage.get("index");

	
	var cls    = _.groupBy(result,"result");
	var total  = 0;
	
	if(selIndex == 1){
		var subResult = _.groupBy(result,function(data){
				total = data.length;
				if((data.ia1 == "AB " || data.ex1 == "AB ")){return "Absent";}
				else if((+data.ia1 + +data.ex1)<50){return "Fail";}
				else if(+data.ex1>35 && (+data.ia1 + +data.ex1)>50 && (+data.ia1 + +data.ex1)<75){return "Pass";}
				else if((+data.ia1 + +data.ex1)>75 && (+data.ia1 + +data.ex1)<90){return "Second";}
				else if((+data.ia1 + +data.ex1)>90 && (+data.ia1 + +data.ex1)<105){return "First";}
				else if((+data.ia1 + +data.ex1)>105){return "Distinction";}
			});
	}
	else if(selIndex == 2){
		var subResult = _.groupBy(result,function(data){
			
			if((data.ia2 == "AB " || data.ex2 == "AB ")){return "Absent";total++;}
			else if((+data.ia2 + +data.ex2)<50 && (data.ia2 == "AB " && data.ex2 == "AB ")){return "Fail";total = total + 1;}
			else if(+data.ex2>35 && (+data.ia2 + +data.ex2)>50 && (+data.ia2 + +data.ex2)<75){return "Pass";total = total + 1;}
			else if((+data.ia2 + +data.ex2)>75 && (+data.ia2 + +data.ex2)<90){return "Second";total = total + 1;}
			else if((+data.ia2 + +data.ex2)>90 && (+data.ia2 + +data.ex2)<105){return "First";total = total + 1;}
			else if((+data.ia2 + +data.ex2)>105){return "Distinction";total++;}
		});
	}
	if(selIndex == 3){
		var subResult = _.groupBy(result,function(data){
				/* total = data.length;
				if((data.ia3 == "AB " || data.ex3 == "AB ")){return "Absent";}
				else if((+data.ia3 + +data.ex3)<50){return "Fail";}
				else if(+data.ex3>35 && (+data.ia3 + +data.ex3)>50 && (+data.ia3 + +data.ex3)<75){return "Pass";}
				else if((+data.ia3 + +data.ex3)>75 && (+data.ia3 + +data.ex3)<90){return "Second";}
				else if((+data.ia3 + +data.ex3)>90 && (+data.ia3 + +data.ex3)<105){return "First";}
				else if((+data.ia3 + +data.ex3)>105){return "Distinction";} */
				if((data.ia3 == "AB " || data.ex3 == "AB ")){return "Absent";total++;}
				else if((+data.ia3 + +data.ex3)<50 && (data.ia3 == "AB " && data.ex3 == "AB ")){return "Fail";total = total + 1;}
				else if(+data.ex3>35 && (+data.ia3 + +data.ex3)>50 && (+data.ia3 + +data.ex3)<75){return "Pass";total = total + 1;}
				else if((+data.ia3 + +data.ex3)>75 && (+data.ia3 + +data.ex3)<90){return "Second";total = total + 1;}
				else if((+data.ia3 + +data.ex3)>90 && (+data.ia3 + +data.ex3)<105){return "First";total = total + 1;}
				else if((+data.ia3 + +data.ex3)>105){return "Distinction";total++;}
			});
	}
	if(selIndex == 4){
		var subResult = _.groupBy(result,function(data){
				total = data.length;
				if((data.ia4 == "AB " || data.ex4 == "AB ")){return "Absent";}
				else if((+data.ia4 + +data.ex4)<50){return "Fail";}
				else if(+data.ex4>35 && (+data.ia4 + +data.ex4)>50 && (+data.ia4 + +data.ex4)<75){return "Pass";}
				else if((+data.ia4 + +data.ex4)>75 && (+data.ia4 + +data.ex4)<90){return "Second";}
				else if((+data.ia4 + +data.ex4)>90 && (+data.ia4 + +data.ex4)<105){return "First";}
				else if((+data.ia4 + +data.ex4)>105){return "Distinction";}
			});
	}
	if(selIndex == 5){
		var subResult = _.groupBy(result,function(data){
				total = data.length;
				if((data.ia5 == "AB " || data.ex5 == "AB ")){return "Absent";}
				else if((+data.ia5 + +data.ex5)<50){return "Fail";}
				else if(+data.ex5>35 && (+data.ia5 + +data.ex5)>50 && (+data.ia5 + +data.ex5)<75){return "Pass";}
				else if((+data.ia5 + +data.ex5)>75 && (+data.ia5 + +data.ex5)<90){return "Second";}
				else if((+data.ia5 + +data.ex5)>90 && (+data.ia5 + +data.ex5)<105){return "First";}
				else if((+data.ia5 + +data.ex5)>105){return "Distinction";}
			});
	}
	if(selIndex == 6){
		var subResult = _.groupBy(result,function(data){
				total = data.length;
				if((data.ia6 == "AB " || data.ex6 == "AB ")){return "Absent";}
				else if((+data.ia6 + +data.ex6)<50){return "Fail";}
				else if(+data.ex6>35 && (+data.ia6 + +data.ex6)>50 && (+data.ia6 + +data.ex6)<75){return "Pass";}
				else if((+data.ia6 + +data.ex6)>75 && (+data.ia6 + +data.ex6)<90){return "Second";}
				else if((+data.ia6 + +data.ex6)>90 && (+data.ia6 + +data.ex6)<105){return "First";}
				else if((+data.ia6 + +data.ex6)>105){return "Distinction";}
			});
	}
	if(selIndex == 7){
		var subResult = _.groupBy(result,function(data){
				total = data.length;
				if((data.ia7 == "AB " || data.ex7 == "AB ")){return "Absent";}
				else if((+data.ia7 + +data.ex7)<50){return "Fail";}
				else if(+data.ex7>35 && (+data.ia7 + +data.ex7)>50 && (+data.ia7 + +data.ex7)<75){return "Pass";}
				else if((+data.ia7 + +data.ex7)>75 && (+data.ia7 + +data.ex7)<90){return "Second";}
				else if((+data.ia7 + +data.ex7)>90 && (+data.ia7 + +data.ex7)<105){return "First";}
				else if((+data.ia7 + +data.ex7)>105){return "Distinction";}
			});
	}
	
	/* var distinction = cls["Distinction"]!=undefined?cls["Distinction"].length:0;
	var firstClass = cls["First Class"]!=undefined?cls["First Class"].length:0;
	var secondClass = cls["Second Class"]!=undefined?cls["Second Class"].length:0;
	var passClass = cls["Pass"]!=undefined?cls["Pass"].length:0;
	var dist = cls["Distinction"]!=undefined?cls["Distinction"].length:0;
	var fail = cls["Fail"]!=undefined?cls["Fail"].length:0; */
	 var distinction = subResult["Distinction"]!=undefined?subResult["Distinction"].length:0;
	var firstClass = subResult["First"]!=undefined?subResult["First"].length:0;
	var secondClass = subResult["Second"]!=undefined?subResult["Second"].length:0;
	var passClass = subResult["Pass"]!=undefined?subResult["Pass"].length:0;
	var fail = subResult["Fail"]!=undefined?subResult["Fail"].length:0;
	var absent = subResult["Absent"]!=undefined?subResult["Absent"].length:0;
	
	total = +distinction + +firstClass + +secondClass + +passClass + +fail + +absent;
	
	$("#total_result").text(total);
	$("#total_dist").text(distinction);
	$("#total_first").text(firstClass);
	$("#total_second").text(secondClass);
	$("#total_pass").text(passClass);
	$("#total_fail").text(fail);
	$("#total_absent").text(absent);
	
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Subject Wise Result Analysis'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Percentage',
            data: [
                ['First  Class',   +firstClass/+total],
                ['Second Class',   +secondClass/+total],
                {
                    name: 'Pass',
                    y: +passClass/+total,
                    sliced: true,
                    selected: true
                },
                ['Fail', +fail/+total],
                ['Distinction', +distinction/+total],
                ['Absent', +absent/+total]
            ]
        }]
    });
});

</script>
</head>
<body>



<div id="container" style="min-width: 600px; height: 600px; max-width: 600px; margin: 0 auto">

</div>

<label>Total Number Of Distinction(Marks > 105)</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:20px " id="total_dist"></label><br>
<label>Total Number Of First Class(Marks > 90 and Marks < 105)</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:23px "id="total_first"></label><br>
<label>Total Number Of Second Class(Marks > 75 and Marks < 90)</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:4px "id="total_second"></label><br>
<label>Total Number Of Pass Class(Marks > 35 and Marks < 75)</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:21px "id="total_pass"></label><br>
<label>Total Number Of Fail(Marks < 35)</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:57px" id="total_fail"></label><br>
<label>Total Number Of Absent(Marks = AB)</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:57px" id="total_absent"></label><br>
<hr>
<label>Total Number Of Results</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:30px " id="total_result"></label><br>

</body>
</html>