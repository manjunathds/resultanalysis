<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jstorage.js"></script>
<script src="js/underscore-min.js"></script>
<script src="js/bootstrap/js/bootstrap.min.js"></script>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<script type="text/javascript">
$(function () {
	
	var result = $.jStorage.get("semResult");
	var semRes    = _.groupBy(result,"sem");
	var classArray = [];
	$.each(semRes,function(i,field){
		$.each(field,function(j,fieldj){
			
			var cls    = _.groupBy(fieldj,"result");
			var total  = result.length;
			
			var distinction = cls["Distinction"]!=undefined?cls["Distinction"].length:0;
			var firstClass = cls["First Class"]!=undefined?cls["First Class"].length:0;
			var secondClass = cls["Second Class"]!=undefined?cls["Second Class"].length:0;
			var FailClass = cls["Fail"]!=undefined?cls["Fail"].length:0;
			var fail = cls["Fail"]!=undefined?cls["Fail"].length:0;
			
			classArray.push({"Fail":fail,"Fail":FailClass,"Second":secondClass,"First":firstClass,"Distinction":distinction});
		});
	});
	
	
	/* $("#total_result").text(total);
	$("#total_dist").text(distinction);
	$("#total_first").text(firstClass);
	$("#total_second").text(secondClass);
	$("#total_Fail").text(FailClass);
	$("#total_fail").text(fail);
 */    
 
 
	$('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Semster Wise  Result Analysis'
        },
        subtitle: {
            text: 'Source: College'
        },
        xAxis: {
            categories: [
                '1st Sem',
                '2nd Sem',
                '3rd Sem',
                '4th Sem',
                '5th Sem',
                '6th Sem'
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Student'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} student</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 4
            }
        },
        series: [{
            name: 'Fail',
            data: [classArray[0].Fail,classArray[1].Fail,classArray[2].Fail,classArray[3].Fail,classArray[4].Fail,classArray[5].Fail]

        }, {
            name: 'Pass',
            data: [classArray[0].Third,classArray[1].Third,classArray[2].Third,classArray[3].Third,classArray[4].Third,classArray[5].Third]

        }, {
            name: 'SecondClass',
            data: [classArray[0].Second,classArray[1].Second,classArray[2].Second,classArray[3].Second,classArray[4].Second,classArray[5].Second]

        }, {
            name: 'FirstClass',
            data: [classArray[0].First,classArray[1].First,classArray[2].First,classArray[3].First,classArray[4].First,classArray[5].First]

        }, {
            name: 'Distinction',
            data: [classArray[0].Distinction,classArray[1].Distinction,classArray[2].Distinction,classArray[3].Distinction,classArray[4].Distinction,classArray[5].Distinction]

        }]
    });
 
 
});

</script>
</head>
<body>



<div id="container" style="min-width: 600px; height: 600px; max-width: 600px; margin: 0 auto">

</div>



</body>
</html>