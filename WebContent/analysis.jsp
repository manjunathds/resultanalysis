<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="../js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jstorage.js"></script>
<script src="js/underscore-min.js"></script>
<script src="../js/bootstrap/js/bootstrap.min.js"></script>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>

<script type="text/javascript">
$(function () {
	
	var result = $.jStorage.get("result");
	var cls    = _.groupBy(result,"result");
	var total  = result.length;
	
	var distinction = cls["Distinction"]!=undefined?cls["Distinction"].length:0;
	var firstClass = cls["First Class"]!=undefined?cls["First Class"].length:0;
	var secondClass = cls["Second Class"]!=undefined?cls["Second Class"].length:0;
	var passClass = cls["Pass"]!=undefined?cls["Pass"].length:0;
	var dist = cls["Distinction"]!=undefined?cls["Distinction"].length:0;
	var fail = cls["Fail"]!=undefined?cls["Fail"].length:0;
	
	$("#total_result").text(total);
	$("#total_dist").text(distinction);
	$("#total_first").text(firstClass);
	$("#total_second").text(secondClass);
	$("#total_pass").text(passClass);
	$("#total_fail").text(fail);
	
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Semester Result Analysis'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Percentage',
            data: [
                ['First  Class',   +firstClass/+total],
                ['Second Class',   +secondClass/+total],
                {
                    name: 'Pass',
                    y: +passClass/+total,
                    sliced: true,
                    selected: true
                },
                ['Fail', +fail/+total],
                ['Distinction', +distinction/+total]
            ]
        }]
    });
});

</script>
</head>
<body>



<div id="container" style="min-width: 600px; height: 600px; max-width: 600px; margin: 0 auto">

</div>

<label>Total Number Of Distinction</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:20px " id="total_dist"></label><br>
<label>Total Number Of First Class</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:23px "id="total_first"></label><br>
<label>Total Number Of Second Class</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:4px "id="total_second"></label><br>
<label>Total Number Of Pass Class</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:21px "id="total_pass"></label><br>
<label>Total Number Of Fail</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:57px" id="total_fail"></label><br>
<hr>
<label>Total Number Of Results</label>&nbsp;&nbsp;:&nbsp;&nbsp;<label style="padding-left:30px " id="total_result"></label><br>

</body>
</html>